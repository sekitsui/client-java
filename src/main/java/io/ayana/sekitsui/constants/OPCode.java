package io.ayana.sekitsui.constants;

/**
 * Sekitsui OPCodes
 */
public enum OPCode {

	/** DISPATCH (server<->client) */
	DISPATCH(0),
	/** QUERY (server<-client) */
	QUERY(1),
	/** META (server<-client) */
	META(2),
	/** ACK (server<->server, server<->client)*/
	ACK(3),
	/** NACK (server<->server, server<->client)*/
	NACK(4),
	/** WAIT (server<->server, server->client)*/
	WAIT(5),
	/** HELLO (server->client)*/
	HELLO(10),
	/** IDENTIFY (server<-client)*/
	IDENTIFY(11),
	/** READY (server->client) */
	READY(12),
	/** HEARTBEAT (server<->client)*/
	HEARTBEAT(19),
	/** HEARTBEACT_ACK (server<->client)*/
	HEARTBEAT_ACK(20);

	private final int code;

	OPCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public static OPCode fromCode(int code) {
		for (OPCode op : values()) {
			if (op.getCode() == code) return op;
		}

		return null;
	}

}
