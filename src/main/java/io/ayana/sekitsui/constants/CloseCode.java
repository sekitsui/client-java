package io.ayana.sekitsui.constants;

/**
 * Sekitsui CloseCodes
 */
public enum CloseCode {

	NORMAL_CLOSE(1000),
	GOING_AWAY(1001),
	PROTOCOL_ERROR(1002),
	UNSUPPORTED_DATA(1003),
	NO_STATUS_RECVD(1005),
	ABNORMAL_CLOSURE(1006),
	INVALID_FRAME_PAYLOAD_DATA(1007),
	POLICY_VIOLATION(1008),
	MESSAGE_TOO_BIG(1009),
	MISSING_EXTENSION(1010),
	INTERNAL_ERROR(1010),
	SERVICE_RESTART(1012),
	TRY_AGAIN_LATER(1013),
	BAD_GATEWAY(1014),
	TLS_HANDSHAKE(1015),

	/**
	 * No idea whats wrong, client should try to reconnect
	 */
	UNKNOWN(4000),
	/**
	 * Client didn't send JSON / missing 'op' and/or 'd' in packet
	 */
	DECODE_ERROR(4001),
	/**
	 * Client sent unknown opcode
	 */
	INVALID_OPCODE(4002),
	/**
	 * Client trying to send other opcodes prior to identifing
	 */
	NOT_IDENTIFIED(4003),
	/**
	 * Client failed to send Identify payload in time
	 */
	IDENTIFY_TIMEOUT(4004),
	/**
	 * Client tried to identify more then once
	 */
	ALREADY_IDENTIFIED(4005),
	/**
	 * Client Session timed out. Possibly caused by not enough hearbeats
	 */
	SESSION_TIMEOUT(4006),
	/**
	 * Client and Server appear to be out of sync, sending improper payloads
	 *
	 * ex. Broker sent Hello twice
	 */
	INVALID_STATE(4007),

	;

	private final int code;

	CloseCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public static CloseCode fromCode(int code) {
		for (CloseCode cc : values()) {
			if (cc.getCode() == code) return cc;
		}

		return null;
	}

}
