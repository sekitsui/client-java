package io.ayana.sekitsui.ws.packet;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import io.ayana.sekitsui.exception.PacketDeserializationException;
import io.ayana.sekitsui.constants.OPCode;
import io.ayana.sekitsui.exception.PacketValidationException;

public abstract class Packet {

	private static final JsonParser parser = new JsonParser();

	// We use a single instance of the PacketInHeartbeat to save some object creations
	private static PacketInHeartbeat pktInHeartbeat;

	private final OPCode opCode;
	private final String type;

	final JsonObject packetData;

	Packet(OPCode opCode, String type) {
		this.opCode = opCode;
		this.type = type;
		this.packetData = new JsonObject();
	}

	Packet(OPCode opCode, String type, JsonObject packetData) {
		this.opCode = opCode;
		this.type = type;
		this.packetData = packetData;
	}

	public OPCode getOpCode() {
		return opCode;
	}

	public String getType() {
		return this.type;
	}

	public String serialize() throws PacketValidationException {
		this.validate();

		JsonObject pkt = new JsonObject();
		pkt.addProperty("op", this.opCode.getCode());
		if (this.type != null) pkt.addProperty("t", this.type);
		pkt.add("d", this.packetData);
		return pkt.toString();
	}

	public static Packet deserialize(String json) throws PacketDeserializationException, PacketValidationException {
		JsonObject pkt;
		try {
			pkt = (JsonObject) Packet.parser.parse(json);
		} catch (JsonSyntaxException e) {
			throw new PacketDeserializationException("Failed to parse JSON", json, e);
		}

		if (!pkt.has("op")) throw new PacketDeserializationException("Packet has no OPCode", json);

		int intOpCode;
		try {
			intOpCode = pkt.get("op").getAsInt();
		} catch (UnsupportedOperationException e) {
			throw new PacketDeserializationException("Packet OPCode was not an int", json, e);
		}

		OPCode opCode = OPCode.fromCode(intOpCode);
		if (opCode == null) throw new PacketDeserializationException("Unknown OPCode: " + intOpCode, json);

		if (opCode == OPCode.HEARTBEAT_ACK) {
			if (pktInHeartbeat == null) pktInHeartbeat = new PacketInHeartbeat();
			return pktInHeartbeat;
		}

		String type = null;
		if (pkt.has("t") && !pkt.get("d").isJsonNull()) {
			try {
				type = pkt.get("t").getAsString();
			} catch (UnsupportedOperationException e) {
				throw new PacketDeserializationException("Packet type was not a String", json, e);
			}
		}

		JsonObject data = null;
		if (pkt.has("d") && !pkt.get("d").isJsonNull()) {
			try {
				data = pkt.get("d").getAsJsonObject();
			} catch (UnsupportedOperationException e) {
				throw new PacketDeserializationException("Packet data was not a JsonObject", json);
			}
		}

		Packet packet;
		switch (opCode) {
			case DISPATCH:
				packet = new PacketInDispatch(type, data);
				break;
			case HELLO:
				packet = new PacketInHello(data);
				break;
			case READY:
				packet = new PacketInReady(data);
				break;
			default:
				throw new PacketDeserializationException("Unknown packet (op: " + opCode + ", t: " + type + ")", json);
		}

		packet.validate();

		return packet;
	}

	abstract void validate() throws PacketValidationException;

	private void validatePropExists(String prop) throws PacketValidationException {
		if (!this.packetData.has(prop)) {
			throw new PacketValidationException(this.getClass().getName() + ": Property \"" + prop + "\" does not exist", this);
		}
	}

	void validatePropString(String prop) throws PacketValidationException {
		this.validatePropExists(prop);

		try {
			this.packetData.get(prop).getAsString();
		} catch (Exception e) {
			throw new PacketValidationException(this.getClass().getName() + ": Property \"" + prop + "\" is not of type String", this, e);
		}
	}

	void validatePropStringOrNull(String prop) throws PacketValidationException {
		this.validatePropExists(prop);

		try {
			if (!this.packetData.get(prop).isJsonNull()) this.packetData.get(prop).getAsString();
		} catch (Exception e) {
			throw new PacketValidationException(this.getClass().getName() + ": Property \"" + prop + "\" is not of type String", this, e);
		}
	}

	void validatePropInt(String prop) throws PacketValidationException {
		this.validatePropExists(prop);

		try {
			this.packetData.get(prop).getAsInt();
		} catch (Exception e) {
			throw new PacketValidationException(this.getClass().getName() + ": Property \"" + prop + "\" is not of type int", this, e);
		}
	}

	void validatePropBoolean(String prop) throws PacketValidationException {
		this.validatePropExists(prop);

		try {
			this.packetData.get(prop).getAsBoolean();
		} catch (Exception e) {
			throw new PacketValidationException(this.getClass().getName() + ": Property \"" + prop + "\" is not of type boolean", this, e);
		}
	}

}
