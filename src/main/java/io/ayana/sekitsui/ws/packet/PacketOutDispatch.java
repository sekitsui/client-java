package io.ayana.sekitsui.ws.packet;

import io.ayana.sekitsui.constants.OPCode;

public final class PacketOutDispatch extends Packet {

	public PacketOutDispatch(String type) {
		super(OPCode.DISPATCH, type);
	}

	@Override
	void validate() {}
}
