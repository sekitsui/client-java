package io.ayana.sekitsui.ws.packet;

import com.google.gson.JsonObject;
import io.ayana.sekitsui.constants.OPCode;

public final class PacketInDispatch extends Packet {

	PacketInDispatch(String type, JsonObject data) {
		super(OPCode.DISPATCH, type, data);
	}

	@Override
	void validate() {}
}
