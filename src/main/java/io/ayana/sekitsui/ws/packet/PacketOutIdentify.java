package io.ayana.sekitsui.ws.packet;

import com.google.gson.JsonObject;
import io.ayana.sekitsui.constants.OPCode;
import io.ayana.sekitsui.exception.PacketValidationException;

/**
 * Outgoing packet for identifying
 */
public final class PacketOutIdentify extends Packet {

	public PacketOutIdentify() {
		super(OPCode.IDENTIFY, null);
	}

	public void setSessionID(String sessionID) {
		this.packetData.addProperty("sessionID", sessionID);
	}

	public void setNamespace(String namespace) {
		this.packetData.addProperty("namespace", namespace);
	}

	public void setPropertyVersion(String version) {
		this.checkPropertiesExist();

		this.packetData.get("properties").getAsJsonObject().addProperty("version", version);
	}

	public void setPropertyLanguage(String language) {
		this.checkPropertiesExist();

		this.packetData.get("properties").getAsJsonObject().addProperty("language", language);
	}

	public void setPropertyPlatform(String platform) {
		this.checkPropertiesExist();

		this.packetData.get("properties").getAsJsonObject().addProperty("platform", platform);
	}

	public void setPropertyArch(String arch) {
		this.checkPropertiesExist();

		this.packetData.get("properties").getAsJsonObject().addProperty("arch", arch);
	}

	private void checkPropertiesExist() {
		if (!this.packetData.has("properties")) this.packetData.add("properties", new JsonObject());
	}

	@Override
	void validate() throws PacketValidationException {
		this.validatePropStringOrNull("sessionID");
		this.validatePropString("namespace");
		this.checkPropertiesExist();
	}
}
