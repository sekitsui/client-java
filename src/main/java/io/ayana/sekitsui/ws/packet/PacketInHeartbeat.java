package io.ayana.sekitsui.ws.packet;

import io.ayana.sekitsui.constants.OPCode;

public final class PacketInHeartbeat extends Packet {

	PacketInHeartbeat() {
		super(OPCode.HEARTBEAT_ACK, null);
	}

	@Override
	void validate() {}
}
