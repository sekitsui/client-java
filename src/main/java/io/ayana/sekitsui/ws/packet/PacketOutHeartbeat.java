package io.ayana.sekitsui.ws.packet;

import io.ayana.sekitsui.constants.OPCode;

public final class PacketOutHeartbeat extends Packet {

	private final String serialized = "{\"op\":" + OPCode.HEARTBEAT.getCode() + ",\"d\":null}";

	public PacketOutHeartbeat() {
		super(OPCode.HEARTBEAT, null);
	}

	@Override
	public String serialize() {
		return serialized;
	}

	@Override
	void validate() {}

}
