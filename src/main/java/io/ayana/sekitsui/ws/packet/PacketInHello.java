package io.ayana.sekitsui.ws.packet;

import com.google.gson.JsonObject;
import io.ayana.sekitsui.constants.OPCode;
import io.ayana.sekitsui.exception.PacketValidationException;

public final class PacketInHello extends Packet {

	PacketInHello(JsonObject data) {
		super(OPCode.HELLO, null, data);
	}

	public String getBrokerID() {
		return this.packetData.get("brokerID").getAsString();
	}

	public String getBrokerVersion() {
		return this.packetData.get("brokerVersion").getAsString();
	}

	public int getHeartbeatInterval() {
		return this.packetData.get("heartbeatInterval").getAsInt();
	}

	@Override
	void validate() throws PacketValidationException {
		this.validatePropString("brokerID");
		this.validatePropString("brokerVersion");
		this.validatePropInt("heartbeatInterval");
	}
}
