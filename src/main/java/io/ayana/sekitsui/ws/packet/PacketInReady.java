package io.ayana.sekitsui.ws.packet;

import com.google.gson.JsonObject;
import io.ayana.sekitsui.constants.OPCode;
import io.ayana.sekitsui.exception.PacketValidationException;

public final class PacketInReady extends Packet {

	PacketInReady(JsonObject data) {
		super(OPCode.READY, null, data);
	}

	public String getSessionID() {
		return this.packetData.get("sessionID").getAsString();
	}

	public boolean isResumed() { return this.packetData.get("resumed").getAsBoolean(); }

	@Override
	void validate() throws PacketValidationException {
		this.validatePropString("sessionID");
		this.validatePropBoolean("resumed");
	}
}
