package io.ayana.sekitsui.ws;

/**
 * Describes the state of a SekitsuiSocket
 */
public enum SocketState {

	/** SekitsuiSocket has been created */
	INITIALIZED,
	/** SekitsuiSocket is connected to the Broker WebSocket */
	CONNECTED,
	/** SekitsuiSocket has sent the IDENTIFY packet to the Broker */
	IDENTIFIED,
	/** SekitsuiSocket received the READY packet from the Broker */
	READY,
	/** SekitsuiSocket got disconnected from the Broker */
	DISCONNECTED,

}
