package io.ayana.sekitsui.ws.listener;

import io.ayana.sekitsui.constants.CloseCode;

@FunctionalInterface
public interface DisconnectListener {

	void onDisconnect(CloseCode code, boolean closedByServer);

}
