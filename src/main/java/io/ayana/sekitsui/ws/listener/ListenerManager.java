package io.ayana.sekitsui.ws.listener;

import java.util.Collection;
import java.util.HashMap;

public final class ListenerManager<T> {

	private int counter = 1;
	private final HashMap<Integer, T> listeners = new HashMap<>();

	public int addListener(T listener) {
		int id = counter++;
		this.listeners.put(id, listener);
		return id;
	}

	public T removeListener(int listenerID) {
		return this.listeners.remove(listenerID);
	}

	public int size() {
		return this.listeners.size();
	}

	public Collection<T> get() {
		return this.listeners.values();
	}

	public void removeAll() {
		this.listeners.clear();
	}

}
