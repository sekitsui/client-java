package io.ayana.sekitsui.ws.listener;

@FunctionalInterface
public interface ExceptionListener {

	void onException(Exception e);

}
