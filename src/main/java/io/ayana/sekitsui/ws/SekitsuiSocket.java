package io.ayana.sekitsui.ws;

import com.neovisionaries.ws.client.*;
import io.ayana.sekitsui.SekitsuiClient;
import io.ayana.sekitsui.constants.CloseCode;
import io.ayana.sekitsui.constants.OPCode;
import io.ayana.sekitsui.dsn.SekitsuiDSN;
import io.ayana.sekitsui.exception.IllegalSocketStateException;
import io.ayana.sekitsui.exception.PacketDeserializationException;
import io.ayana.sekitsui.exception.PacketValidationException;
import io.ayana.sekitsui.exception.UnexpectedOPCodeException;
import io.ayana.sekitsui.ws.listener.DisconnectListener;
import io.ayana.sekitsui.ws.listener.ExceptionListener;
import io.ayana.sekitsui.ws.listener.ListenerManager;
import io.ayana.sekitsui.ws.packet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ConnectException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public final class SekitsuiSocket {

	private static final Logger logger = LoggerFactory.getLogger(SekitsuiSocket.class);
	private static final WebSocketFactory wsFactory;

	static {
		wsFactory = new WebSocketFactory();
		wsFactory.setConnectionTimeout(15000);
	}

	private final SekitsuiClient cli;
	private final Consumer<Exception> onSocketFinalize;

	private String sessionID = null;

	private final WebSocket socket;
	private SocketState state = SocketState.INITIALIZED;

	private Thread heartbeatThread = null;
	private long lastHeartbeatSent = 0;
	private long lastHearbeatReceived = 0;
	private final ArrayList<Integer> latency = new ArrayList<>(5);

	private final ListenerManager<ExceptionListener> exceptionListeners = new ListenerManager<>();
	private final ListenerManager<DisconnectListener> disconnectListeners = new ListenerManager<>();

	public SekitsuiSocket(SekitsuiClient cli, SekitsuiDSN dsn, Consumer<Exception> onSocketFinalize) throws IOException {
		this.cli = cli;
		this.onSocketFinalize = onSocketFinalize;

		this.socket = wsFactory.createSocket(dsn.getSocketURL());
		this.socket.addListener(new SocketListener());
	}

	public String getSessionID() {
		return this.sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	public synchronized void connect() {
		if (this.state != SocketState.INITIALIZED) {
			throw new IllegalSocketStateException("Cannot reconnect with the same SekitsuiSocket", this.state);
		}

		logger.debug("Attempting connect");
		this.socket.connectAsynchronously();
	}

	public synchronized void disconnect(CloseCode code) {
		this.disconnect(code, "");
	}

	public synchronized void disconnect(CloseCode code, String reason) {
		if (this.state == SocketState.INITIALIZED || this.state == SocketState.DISCONNECTED) return;

		logger.debug("Attempting disconnect");
		this.socket.disconnect(code.getCode(), reason);
	}

	public void send(Packet packet) throws PacketValidationException {
		this.send(packet.serialize());
	}

	public void sendNoTrace(Packet packet) throws PacketValidationException {
		this.sendNoTrace(packet.serialize());
	}

	private void send(String json) {
		logger.trace(">> " + json);
		this.sendNoTrace(json);
	}

	private void sendNoTrace(String json) {
		this.socket.sendText(json);
	}

	public int onDisconnect(DisconnectListener listener) {
		return this.disconnectListeners.addListener(listener);
	}

	public DisconnectListener removeDisconnetListener(int listenerID) {
		return this.disconnectListeners.removeListener(listenerID);
	}

	public int onException(ExceptionListener listener) {
		return this.exceptionListeners.addListener(listener);
	}

	public ExceptionListener removeExceptionListener(int listenerID) {
		return this.exceptionListeners.removeListener(listenerID);
	}

	public void removeAllListeners() {
		this.disconnectListeners.removeAll();
		this.exceptionListeners.removeAll();
	}

	private void handleHello(PacketInHello packet) throws PacketValidationException {
		if (this.state != SocketState.CONNECTED) {
			this.disconnect(CloseCode.INVALID_STATE, "Unexpected HELLO packet");
			this.handleException(new IllegalSocketStateException("Socket wasn't in state CONNECTED while receiving a HELLO packet", this.state));
		}

		logger.debug("Received HELLO packet from Broker");
		logger.debug("brokerID = " + packet.getBrokerID() + ", brokerVersion = " + packet.getBrokerVersion() + ", heartbeatInterval = " + packet.getHeartbeatInterval());

		PacketOutIdentify identify = new PacketOutIdentify();
		identify.setSessionID(this.sessionID);
		identify.setNamespace(this.cli.getCurrentDSN().getNamespace());
		identify.setPropertyVersion("0.0.1");
		identify.setPropertyLanguage("Java " + System.getProperty("java.version"));
		identify.setPropertyPlatform(System.getProperty("os.name"));
		identify.setPropertyArch(System.getProperty("os.arch"));

		this.state = SocketState.IDENTIFIED;
		this.send(identify);
		logger.debug("Sent IDENTIFY to Broker");

		logger.debug("Starting HeartbeatThread with " + packet.getHeartbeatInterval() + "ms interval");

		final int heartbeatInterval = packet.getHeartbeatInterval();
		final int heartbeatTimeout = heartbeatInterval * 3 - 2000;
		final PacketOutHeartbeat hbeat = new PacketOutHeartbeat();
		this.heartbeatThread = new Thread(() -> {
			Thread.currentThread().setName("HeartbeatThread");
			this.lastHearbeatReceived = System.currentTimeMillis();

			while (this.state != SocketState.DISCONNECTED) {
				try {
					Thread.sleep(heartbeatInterval);
				} catch (InterruptedException e) {
					return;
				}

				if (this.lastHearbeatReceived + heartbeatTimeout < System.currentTimeMillis()) {
					logger.warn("Broker didn't send a heartbeat after waiting " + (System.currentTimeMillis() - this.lastHearbeatReceived) + "ms");
					this.disconnect(CloseCode.SESSION_TIMEOUT, "Broker took too long to send heartbeat");
					return;
				}

				if (this.state != SocketState.DISCONNECTED) {
					this.lastHeartbeatSent = System.currentTimeMillis();
					try {
						this.sendNoTrace(hbeat);
					} catch (PacketValidationException e) {
						// Can never be thrown by a PacketOutHeartbeat
					}
				}
			}
		});

		this.heartbeatThread.start();
	}

	private void handleReady(PacketInReady packet) {
		if (this.state != SocketState.IDENTIFIED) {
			this.disconnect(CloseCode.INVALID_STATE, "Unexpected READY packet");
			this.handleException(new IllegalSocketStateException("Socket wasn't in state IDENTIFIED while receiving a READY packet", this.state));
		}

		logger.debug("Received READY packet from Broker");
		logger.debug("sessionID = " + packet.getSessionID() + ", resumed = " + packet.isResumed());

		this.sessionID = packet.getSessionID();

		logger.info("Connected to Sekitsui Broker with sessionID " + this.sessionID);
		this.state = SocketState.READY;
	}

	private void handleException(Exception e) {
		if (this.exceptionListeners.size() > 0) {
			for (ExceptionListener l : this.exceptionListeners.get()) {
				l.onException(e);
			}
			return;
		}

		logger.error("Unhandled exception in SekitsuiSocket", e);
	}

	private class SocketListener extends WebSocketAdapter {

		@Override
		public void onConnectError(WebSocket websocket, WebSocketException exception) {
			SekitsuiSocket.this.onSocketFinalize.accept(exception);
		}

		@Override
		public void onDisconnected(WebSocket websocket, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) {
			CloseCode code = CloseCode.fromCode(clientCloseFrame.getCloseCode());

			if (closedByServer) {
				logger.warn("Server disconnected us with code " + code + " (" + clientCloseFrame.getCloseCode() + ")");
			} else {
				logger.warn("Closed WebSocket connection with code " + code + " (" + clientCloseFrame.getCloseCode() + ")");
			}

			SekitsuiSocket.this.state = SocketState.DISCONNECTED;
			SekitsuiSocket.this.heartbeatThread.interrupt();
			SekitsuiSocket.this.heartbeatThread = null;

			try {
				for (DisconnectListener l : disconnectListeners.get()) {
					l.onDisconnect(code, closedByServer);
				}
			} catch (Exception e) {
				SekitsuiSocket.this.handleException(e);
			}

			SekitsuiSocket.this.onSocketFinalize.accept(null);
		}

		@Override
		public void onConnected(WebSocket websocket, Map<String, List<String>> headers) {
			logger.debug("Established connection to Broker");
			SekitsuiSocket.this.state = SocketState.CONNECTED;
		}

		@Override
		public void onError(WebSocket websocket, WebSocketException cause) {
			if (cause.getCause() != null && cause.getCause().getClass() == ConnectException.class) return;

			SekitsuiSocket.this.handleException(cause);
		}

		@Override
		public void onTextMessage(WebSocket websocket, String text) throws Exception {
			if(!text.startsWith("{\"op\":" + OPCode.HEARTBEAT_ACK.getCode())) logger.trace("<< " + text);

			Packet pkt;
			try {
				pkt = Packet.deserialize(text);
			} catch (PacketDeserializationException | PacketValidationException e) {
				SekitsuiSocket.this.disconnect(CloseCode.DECODE_ERROR);
				SekitsuiSocket.this.handleException(e);
				return;
			}

			switch (pkt.getOpCode()) {
				case HEARTBEAT_ACK:
					SekitsuiSocket.this.lastHearbeatReceived = System.currentTimeMillis();

					int latency = (int) (SekitsuiSocket.this.lastHearbeatReceived - SekitsuiSocket.this.lastHeartbeatSent);
					if (SekitsuiSocket.this.latency.size() >= 5) SekitsuiSocket.this.latency.remove(0);
					SekitsuiSocket.this.latency.add(latency);
					break;
				case DISPATCH:

					break;
				case HELLO:
					SekitsuiSocket.this.handleHello((PacketInHello) pkt);
					break;
				case READY:
					SekitsuiSocket.this.handleReady((PacketInReady) pkt);
					break;
				default:
					SekitsuiSocket.this.disconnect(CloseCode.INVALID_OPCODE);
					SekitsuiSocket.this.handleException(new UnexpectedOPCodeException("Unexpected OPCode in receive method", pkt.getOpCode()));
					break;
			}
		}

		@Override
		public void handleCallbackError(WebSocket websocket, Throwable cause) {
			logger.error("Unexpected unhandled error in SekitsuiSocket", cause);
		}

	}

}
