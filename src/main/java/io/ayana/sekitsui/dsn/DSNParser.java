package io.ayana.sekitsui.dsn;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

public final class DSNParser {

	private DSNParser() {}

	public static SekitsuiDSN parse(String dsn) throws URISyntaxException, MalformedURLException, UnsupportedEncodingException {
		URI url = new URI(dsn);

		String host = url.getHost();
		if (host == null) host = "127.0.0.1";
		int port = url.getPort();
		if (port == -1) port = 27532;

		Map<String, String> query = parseQuery(url);

		String namespace = query.get("namespace");
		if (namespace == null) throw new MalformedURLException("SekitsuiDSN must contain \"namespace\" property");

		boolean insecure = query.get("insecure").equals("true");

		return new SekitsuiDSN(host, port, namespace, insecure);
	}

	private static Map<String, String> parseQuery(URI url) throws UnsupportedEncodingException {
		Map<String, String> queryPairs = new LinkedHashMap<>();
		String query = url.getQuery();
		String[] pairs = query.split("&");

		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			queryPairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
		}

		return queryPairs;
	}

}
