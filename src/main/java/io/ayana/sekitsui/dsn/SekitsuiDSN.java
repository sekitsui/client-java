package io.ayana.sekitsui.dsn;

public final class SekitsuiDSN {

	private final String socketUrl;

	private final String host;
	private final int port;
	private final String namespace;
	private final String protocol;

	SekitsuiDSN(String host, int port, String namespace, boolean insecure) {
		this.host = host;
		this.port = port;
		this.namespace = namespace;
		this.protocol = insecure ? "ws" : "wss";

		this.socketUrl = this.protocol + "://" + this.host + ":" + this.port;
	}

	public String getSocketURL() {
		return this.socketUrl;
	}

	public String getHost() {
		return this.host;
	}

	public int getPort() {
		return this.port;
	}

	public String getNamespace() {
		return this.namespace;
	}

	public String getProtocol() {
		return this.protocol;
	}
}
