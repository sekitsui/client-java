package io.ayana.sekitsui;

import io.ayana.sekitsui.constants.CloseCode;
import io.ayana.sekitsui.dsn.DSNParser;
import io.ayana.sekitsui.dsn.SekitsuiDSN;
import io.ayana.sekitsui.ws.SekitsuiSocket;
import io.ayana.sekitsui.ws.listener.DisconnectListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * SekitsuiClient
 */
public final class SekitsuiClient {

	private static final Logger logger = LoggerFactory.getLogger(SekitsuiClient.class);

	private SekitsuiDSN dsn;

	private SekitsuiSocket socket;
	private String lastSessionID;

	private boolean autoReconnect = true;
	private Thread reconnectThread;
	private final Runnable reconnect;
	private final Runnable doReconnect;

	/**
	 * Creates a new SekitsuiClient object
	 */
	public SekitsuiClient() {
		this.reconnect = () -> {
			Thread.currentThread().setName("ReconnectThread");

			synchronized (this) {
				// Save sessionID from previous socket
				if (this.socket != null) {
					this.lastSessionID = this.socket.getSessionID();
					this.socket = null;
				}
			}

			logger.info("Waiting 5000ms before reconnecting...");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				return;
			}

			synchronized (this) {
				// Don't connect if autoReconnect has been set to false after the sleep
				if (!this.autoReconnect) return;

				try {
					this.connect();
				} catch (Exception e) {
					// TODO Probably error out completely here
					logger.error("Failed reconnect", e);
				}

				this.reconnectThread = null;
			}
		};

		this.doReconnect = () -> {
			synchronized (this) {
				// We don't want to auto-reconnect if it has been disabled
				if (!this.autoReconnect) return;

				if (this.reconnectThread == null) {
					this.reconnectThread = new Thread(this.reconnect);
					this.reconnectThread.start();
				}
			}
		};
	}

	/**
	 * Connects to Sekitsui with the preexisting DSN in this object
	 *
	 * @throws IOException When the WebSocket creation fails
	 * @throws URISyntaxException When the given DSN is invalid
	 * @throws IllegalArgumentException When there is no preexisting DSN in this object
	 */
	public synchronized void connect() throws IOException, URISyntaxException {
		this.connect(null);
	}

	/**
	 * Connects to Sekitsui with the specified DSN
	 *
	 * @param dsn The Sekitsui DSN. Format:  {@code sekitsui://<host>[:<port>]/?namespace=<namespace>[&insecure=<boolean>][&auth=<auth>]}
	 *
	 * @throws IOException When the WebSocket creation fails
	 * @throws URISyntaxException When the given DSN is invalid
	 * @throws IllegalStateException When there is still a connection open
	 * @throws IllegalArgumentException When the DSN is null and there is no DSN set in this object
	 */
	public synchronized void connect(String dsn) throws IOException, URISyntaxException, IllegalStateException, IllegalArgumentException {
		if (this.socket != null) throw new IllegalStateException("Cannot connect while still being connected");

		// Check if there is a DSN passed and parse it
		if (dsn != null) this.dsn = DSNParser.parse(dsn);
		// Check if the DSN in this object is not null. If it is then we can't continue
		if (this.dsn == null) throw new IllegalArgumentException("Sekitsui DSN must be set as least once");

		// Create the client with a consumer that is executed when the socket is finalized
		logger.debug("Creating SekitsuiSocket");
		this.socket = new SekitsuiSocket(this, this.dsn, e -> {
			if (e != null) logger.warn(e.getMessage());
			doReconnect.run();
		});

		// Apply the last session ID for resuming the connection
		this.socket.setSessionID(this.lastSessionID);

		// Connect
		logger.debug("Connecting to " + this.dsn.getSocketURL());
		this.socket.connect();
	}

	/**
	 * Disconnects the client if a connection exists
	 */
	public synchronized void disconnect() {
		this.disconnect(null);
	}

	/**
	 * Disconnects the client if a connection exists and calls onDisconnected when finished
	 *
	 * @param onDisconnected Called when the connection has been disconnected. If there was no connection there will be no CloseCode passed
	 */
	public synchronized void disconnect(DisconnectListener onDisconnected) {
		this.autoReconnect = false;

		if (this.reconnectThread != null) {
			this.reconnectThread.interrupt();
			this.reconnectThread = null;
		}

		if (this.socket == null) {
			onDisconnected.onDisconnect(null, false);
			return;
		}

		if (onDisconnected != null) {
			this.socket.onDisconnect(onDisconnected);
		}

		this.socket.disconnect(CloseCode.NORMAL_CLOSE, "Goodbye");
		this.socket.removeAllListeners();
		this.socket = null;
	}

	/**
	 * Returns the current DSN
	 *
	 * @return The current DSN
	 */
	public synchronized SekitsuiDSN getCurrentDSN() {
		return this.dsn;
	}

}
