package io.ayana.sekitsui.exception;

import io.ayana.sekitsui.ws.SocketState;

public final class IllegalSocketStateException extends IllegalStateException {

	private final String message;
	private final Throwable cause;

	private final SocketState state;

	public IllegalSocketStateException(String message, SocketState state) {
		this(message, state, null);
	}

	public IllegalSocketStateException(String message, SocketState state, Throwable cause) {
		this.message = message;
		this.cause = cause;

		this.state = state;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public synchronized Throwable getCause() {
		return cause;
	}

	public SocketState getState() {
		return state;
	}
}
