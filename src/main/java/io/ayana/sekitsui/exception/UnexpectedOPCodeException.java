package io.ayana.sekitsui.exception;

import io.ayana.sekitsui.constants.OPCode;

public final class UnexpectedOPCodeException extends Exception {

	private final String message;
	private final Throwable cause;

	private final OPCode opCode;

	public UnexpectedOPCodeException(String message, OPCode opCode) {
		this(message, opCode, null);
	}

	public UnexpectedOPCodeException(String message, OPCode opCode, Throwable cause) {
		this.message = message;
		this.cause = cause;

		this.opCode = opCode;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public synchronized Throwable getCause() {
		return cause;
	}

	public OPCode getOpCode() {
		return opCode;
	}
}
