package io.ayana.sekitsui.exception;

import io.ayana.sekitsui.ws.packet.Packet;

public final class PacketValidationException extends Exception {

	private final String message;
	private final Throwable cause;

	private final Packet packet;

	public PacketValidationException(String message, Packet packet) {
		this(message, packet, null);
	}

	public PacketValidationException(String message, Packet packet, Throwable cause) {
		this.message = message;
		this.cause = cause;

		this.packet = packet;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public synchronized Throwable getCause() {
		return cause;
	}

	public Packet getPacket() {
		return packet;
	}
}
