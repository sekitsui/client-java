package io.ayana.sekitsui.exception;

public final class PacketDeserializationException extends Exception {

	private final String message;
	private final Throwable cause;

	private final String serializedPacket;

	public PacketDeserializationException(String message, String serializedPacket) {
		this(message, serializedPacket, null);
	}

	public PacketDeserializationException(String message, String serializedPacket, Throwable cause) {
		this.message = message;
		this.cause = cause;

		this.serializedPacket = serializedPacket;
	}

	@Override
	public String getMessage() {
		return message;
	}

	@Override
	public synchronized Throwable getCause() {
		return cause;
	}

	public String getSerializedPacket() {
		return serializedPacket;
	}
}
