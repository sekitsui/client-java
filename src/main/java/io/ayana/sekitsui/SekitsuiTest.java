package io.ayana.sekitsui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SekitsuiTest {

	private static final Logger logger = LoggerFactory.getLogger(SekitsuiTest.class);

	public static void main(String[] args) {
		logger.info("Starting test application...");
		SekitsuiClient cli = new SekitsuiClient();
		try {
			cli.connect("sekitsui://localhost/?insecure=true&namespace=test");
		} catch (Exception e) {
			logger.error("Dick caught in ceiling fan", e);
		}
	}

}
