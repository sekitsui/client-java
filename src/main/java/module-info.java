module io.ayana.sekitsui {
	requires java.base;

	requires slf4j.api;
	requires nv.websocket.client;
	requires gson;

	exports io.ayana.sekitsui;
}
